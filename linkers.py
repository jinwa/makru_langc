from typing import Iterable, Optional
from makru import panic
from makru.helpers import shell, check_binary
from . import LinkerFlags, PluginGlobalState, Target, generate_shell_show, Linker


class CommonCompilerBuiltinLinker(Linker):
    def __init__(self, path: str = None) -> None:
        self.path = path if path else check_binary("cc")

    def link(
        self, files: Iterable[str], output_name: str, flags: LinkerFlags, dryrun=False
    ):
        if flags.target:
            panic("CommonCompilerNuiltinLinker does not support cross-compiling")
        command = [
            self.path,
            *files,
            "-o",
            output_name,
            *map(lambda x: "-Xlinker {}".format(x), flags.ldflags),
        ]
        if flags.shared:
            command.insert(2, "-shared")
        if dryrun:
            print(generate_shell_show(command))
            return 0
        else:
            return shell(command)

    @classmethod
    def is_avaliable(cls, *, target: Optional[Target]):
        if target:
            return False
        return check_binary("cc") != None


class LDLinker(Linker):
    def __init__(self, path: str = None) -> None:
        self.path = path if path else check_binary("ld")

    def link(
        self, files: Iterable[str], output_name: str, flags: LinkerFlags, dryrun=False
    ):
        command = [self.path, "-o", output_name, *flags.ldflags, *files]
        if flags.shared:
            command.insert(2, "-shared")
        if flags.target:
            if flags.target.triplet.arch:
                command.insert(2, "-A {}".format(flags.target.triplet.arch))
        if dryrun:
            print(generate_shell_show(command))
            return 0
        else:
            return shell(command)

    @classmethod
    def is_avaliable(cls, *, target: Optional[Target]):
        return check_binary("ld") != None


class StaticLibraryArchiveLinker(Linker):
    def __init__(self, path: str = None) -> None:
        self.path = path if path else check_binary("ar")

    def link(
        self, files: Iterable[str], output_name: str, flags: LinkerFlags, dryrun=False
    ):
        command = [self.path, *flags.ldflags, "-rv", output_name, *files]
        if dryrun:
            print(generate_shell_show(command))
            return 0
        else:
            return shell(command)

    @classmethod
    def is_avaliable(cls, *, target: Optional[Target]):
        return check_binary("ar") != None


class ClangBuiltinLinker(Linker):
    def __init__(self, path: str = None) -> None:
        self.path = path if path else check_binary("clang")

    def link(
        self, files: Iterable[str], output_name: str, flags: LinkerFlags, dryrun=False
    ):
        command = [
            self.path,
            *files,
            "-o",
            output_name,
            *map(lambda x: "-Xlinker {}".format(x), flags.ldflags),
        ]
        if flags.shared:
            command.insert(2, "-shared")
        if flags.target:
            command.append(
                "--target={}".format(Target.triplet2str(flags.target.triplet))
            )
        if dryrun:
            print(generate_shell_show(command))
            return 0
        else:
            return shell(command)

    @classmethod
    def is_avaliable(cls, *, target: Optional[Target]):
        return check_binary("clang") != None


def langc_startup(pstate: PluginGlobalState):
    pstate.register_linker("ld", LDLinker)
    pstate.register_linker("clang", ClangBuiltinLinker)
    pstate.register_linker("cc", CommonCompilerBuiltinLinker)
    pstate.register_archiver("ar", StaticLibraryArchiveLinker)
