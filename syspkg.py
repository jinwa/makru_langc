import os
from pathlib import Path
import platform
import sys
from typing import Dict, List, Optional
from makru import CompilingDesc, Depend
from . import DependencyResolver, PluginGlobalState, ResolvedDependency, Target
from glob import glob


class ResolvedSystemPackage(ResolvedDependency):
    def __init__(
        self,
        include: str,
        libs: List[str],
        lib_searchs: List[str],
        use_system_package=False,
        depend: Depend = None,
    ) -> None:
        self.include = include
        self.libs = libs
        self.lib_searchs = lib_searchs
        self.use_system_package = use_system_package
        super().__init__(depend)

    def get_include(self):
        return self.include

    def get_libs(self):
        return self.libs

    def get_lib_search_paths(self):
        return self.lib_searchs

    def __str__(self) -> str:
        return "ResolvedSystemPackage(include={} libs={} lib_searchs={}{})".format(
            self.include,
            ", ".join(self.libs),
            ", ".join(self.lib_searchs),
            " use-system-package" if self.use_system_package else "",
        )


class SystemPackageResolver(DependencyResolver):
    def __init__(self, includes: List[str], libraries: List[str]) -> None:
        self.includes = includes
        self.libraries = libraries

    @staticmethod
    def get_lib_name(name: str):
        syss = platform.system()
        os_name = os.name
        if syss == "Linux" or os_name == "posix":
            return "lib{}.so".format(name)
        elif syss == "Darwin" or os_name == "darwin":
            return "{}.dylib"
        elif syss == "Windows":
            return "{}.dll"
        else:
            return None

    def resolve(
        self,
        desc: CompilingDesc,
        dep: Depend,
        target: Optional["Target"] = None,
        dryrun=False,
    ) -> Optional["ResolvedDependency"]:
        if target:
            return None  # Most system won't store cross-platform library under their direcotry
        allow_use_syspkg = dep.name in desc.gvars["use-system-package"]
        if dep.version and not allow_use_syspkg:
            return None  # We could not know the package version in portable way
        include = self.search_includes(dep.name)
        if not include:
            return None
        libname = self.get_lib_name(dep.name)
        if not libname:
            return None
        libpath = self.search_library(libname)
        if not libpath:
            return None
        return ResolvedSystemPackage(
            include,
            [dep.name],
            [libpath],
            use_system_package=allow_use_syspkg,
            depend=dep,
        )

    def search_includes(self, name: str):
        for path in self.includes:
            result = glob("{}{sep}{}.h".format(path, name, sep=os.sep))
            if len(result) > 0:
                return path

    def search_library(self, filename):
        for path in self.libraries:
            result = glob("{}{sep}{}".format(path, filename, sep=os.sep))
            if len(result) > 0:
                return path

    def check_avaliablity(self, desc: CompilingDesc, dryrun=False) -> bool:
        for p in self.includes:
            if not Path(p).exists():
                return False
        for p in self.libraries:
            if not Path(p).exists():
                return False
        return True


LINUX_LNC_DIRS = ["/usr/include", "/usr/local/include"]
LINUX_LIB_DIRS = ["/usr/lib", "/usr/local/lib"]
LINUX_LIB64_DIRS = ["/usr/lib64", "/usr/local/lib64"]


class DefaultLibcResolver(DependencyResolver):
    def check_avaliablity(self, desc: CompilingDesc, dryrun=False) -> bool:
        return True

    def resolve(
        self,
        desc: CompilingDesc,
        name: Depend,
        target: Optional["Target"] = None,
        dryrun: bool = None,
    ) -> Optional["ResolvedDependency"]:
        if name.name != "c" or name.version:
            return None
        langc_conf = desc.config.get("langc")
        if isinstance(langc_conf, Dict) and (langc_conf.get("default_libc") == False):
            return None
        if target:
            return None
        return ResolvedDependency(name, libs=["c"])


def get_exist_paths(paths):
    for p in paths:
        if Path(p).exists():
            yield (p)


def langc_startup(pstate: PluginGlobalState):
    includes = []
    libraries = []
    if platform.system() == "Linux":
        includes += LINUX_LNC_DIRS
        if sys.maxsize > 2 ** 32:
            libraries += LINUX_LIB64_DIRS
        libraries += LINUX_LIB_DIRS
    includes = list(get_exist_paths(includes))
    libraries = list(get_exist_paths(libraries))
    pstate.register_dependency_resolver(
        "system", SystemPackageResolver(includes, libraries)
    )
    pstate.register_dependency_resolver("default_libc", DefaultLibcResolver())
