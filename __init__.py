from functools import cached_property
from typing import Any, Dict, Iterable, Iterator, List, Optional, Protocol, Tuple, Type
from makru import (
    CompilingDesc,
    panic,
    Depend,
    Makru,
    Sandbox,
    true_or_panic,
    OptionDescription,
    panic,
)
from makru.store import Store
from makru.helpers import check_binary, create_makru
from pathlib import Path
from glob import glob
from collections import OrderedDict, namedtuple


class DependencyResolver(object):
    def resolve(
        self,
        desc: CompilingDesc,
        name: Depend,
        target: Optional["Target"] = None,
        dryrun: bool = False,
    ) -> Optional["ResolvedDependency"]:
        pass

    def check_avaliablity(self, desc: CompilingDesc, dryrun=False) -> bool:
        return False


class PluginGlobalState(object):
    def __init__(self):
        self.dependency_resolvers: Dict[str, DependencyResolver] = {}
        self.compilers: OrderedDict[str, Type["Compiler"]] = OrderedDict()
        self.linkers: OrderedDict[str, Type["Linker"]] = OrderedDict()
        self.archivers: OrderedDict[str, Type["Linker"]] = OrderedDict()
        self.workflows: Dict[str, "Workflow"] = {}

    def register_dependency_resolver(self, name: str, resolver: DependencyResolver):
        self.dependency_resolvers[name] = resolver

    def register_compiler(self, name: str, compiler: Type["Compiler"]):
        self.compilers[name] = compiler

    def register_linker(self, name: str, linker: Type["Linker"]):
        self.linkers[name] = linker

    def register_archiver(self, name: str, archiver: Type["Linker"]):
        self.archivers[name] = archiver

    def register_workflow(self, name: str, workflow: "Workflow"):
        self.workflows[name] = workflow


GLOBALSTATE = PluginGlobalState()


class CompilingFlags(object):
    def __init__(
        self,
        *,
        PIC: bool = False,
        includes: Iterable[str] = [],
        libs: Iterable[str] = [],
        libsearchs: Iterable[str] = [],
        cwd: str = None,
        ccflags: Iterable[str] = [],
        environment: Dict[str, str] = None,
        c_std: str = None,
        target: "Target" = None
    ) -> None:
        self.PIC = PIC
        self.includes = includes
        self.libs = libs
        self.libsearchs = libsearchs
        self.cwd = cwd
        self.ccflags = ccflags
        self.environment = environment
        self.c_std = c_std
        self.target = target


class Compiler(Protocol):
    def __init__(self, *args, **kwargs) -> None:
        ...

    def compile(
        self, source_file: str, output_name: str, flags: CompilingFlags, dryrun=False
    ) -> int:
        ...

    @classmethod
    def is_avaliable(cls, *, target: Optional["Target"]):
        ...


class LinkerFlags(object):
    def __init__(
        self, *, shared=False, ldflags: Iterable[str], target: "Target" = None
    ) -> None:
        self.shared = shared
        self.ldflags = ldflags
        self.target = target


class Linker(Protocol):
    def __init__(self, *args, **kwargs) -> None:
        ...

    def link(
        self, files: Iterable[str], output_name: str, flags: LinkerFlags, dryrun=False
    ) -> int:
        ...

    @classmethod
    def is_avaliable(cls, *, target: Optional["Target"]):
        ...


class Workflow(object):
    def enter(self, project_type: str, state: "CompilingState"):
        pass

    def get_id(self, project_type: str, state: "CompilingState") -> str:
        return str(id(self))

    def run(self, project_type: str, state: "CompilingState"):
        panic("action 'run' is not supported by this workflow")


TargetTriplet = namedtuple(
    "TargetTriplet",
    ["arch", "vendor", "sys", "abi"],
    defaults=["unknown", "unknown", "unknown", "unknown"],
)


class Target(object):
    def __init__(self, triplet: TargetTriplet) -> None:
        self.triplet = triplet

    @staticmethod
    def triplet2str(triplet: TargetTriplet) -> str:
        return "-".join(triplet)

    @staticmethod
    def parse_triplet(s: str) -> TargetTriplet:
        parts = s.split("-")
        true_or_panic(len(parts) >= 3, "triplet does not accepted: {}".format(s))
        return TargetTriplet(*parts)


def generate_shell_show(command, cwd=None, env: Dict[str, Any] = None):
    show = []
    if cwd:
        show.append("PWD={}".format(cwd))
    if env:
        for k in env:
            show.append("{}={}".format(k, env[k]))
    show += command
    return " ".join(command)


class ResolvedDependency(object):
    def __init__(
        self,
        depend: Depend,
        ccflags: Optional[List[str]] = None,
        ldflags: Optional[List[str]] = None,
        include: Optional[str] = None,
        libs: Optional[List[str]] = None,
        object_files: Optional[List[str]] = None,
        lib_search_paths: Optional[List[str]] = None,
        dep_id: Optional[str] = None,
    ) -> None:
        self.depend: Depend = depend
        self._ccflags = ccflags if ccflags else []
        self._ldflags = ldflags if ldflags else []
        self._include = include
        self._libs = libs if libs else []
        self._object_files = object_files if object_files else []
        self._lib_search_paths = lib_search_paths if lib_search_paths else []
        self._id = dep_id if dep_id else str(id(self))

    def do_build(self, **kwargs):
        pass

    def get_ccflags(self) -> List[str]:
        return self._ccflags

    def get_ldflags(self) -> List[str]:
        return self._ldflags

    def get_include(self) -> Optional[str]:
        return self._include

    def get_libs(self) -> List[str]:
        return self._libs

    def get_object_files(self):
        return self._object_files

    def get_lib_search_paths(self):
        return self._lib_search_paths

    def get_id(self) -> str:
        return self._id

    def __str__(self) -> str:
        return "ResolvedDependency({depend}, {id})".format(
            depend=self.depend, id=self.get_id()
        )


class ResolvedPathDependency(ResolvedDependency):
    def __init__(
        self,
        project_path: str,
        static_library=True,
        dryrun=False,
        vars={},
        depend: Depend = None,
        target: Target = None,
    ) -> None:
        self.root_path = project_path
        self.include: str
        self.ccflags: List[str]
        self.ldflags: List[str]
        self.static_library = static_library
        self.dryrun = dryrun
        self.vars = vars
        self.depend = depend
        self.target = target
        self.dependency_id: Optional[str] = None

    def get_project(self, rebuild_all=False) -> Makru:
        project: Makru = create_makru("-F{}".format(self.root_path))
        project.globalconfig["vars"].update(self.vars)
        project.globalconfig["vars"]["static-library"] = self.static_library
        project.globalconfig["vars"]["build-path"] = str(
            Path(project.build_sandbox().root) / "build"
        )  # TODO: Hard-coded path
        project.globalconfig["vars"]["dryrun"] = self.dryrun
        if rebuild_all:
            project.globalconfig["vars"]["rebuild-all"] = True
        if self.target:
            project.globalconfig["vars"]["target"] = Target.triplet2str(
                self.target.triplet
            )
        return project

    def do_build(self, **kwargs):
        rebuild_all = kwargs.get("rebuild-all", False)
        project = self.get_project(rebuild_all=rebuild_all)
        project.compile()
        self.include = project.action("_asdependency_include")
        self.ccflags = project.action("_asdependency_ccflags")
        self.ldflags = project.action("_asdependency_ldflags")
        self.libs = project.action("_asdependency_libs")
        self.libsearchpaths = project.action("_asdependency_libsearchpaths")
        self.object_files = project.action("_asdependency_object_files")
        self.dependency_id = project.action(
            "_asdependency_id"
        )  # we should not create a new instance if it already exists

    def get_ccflags(self):
        return self.ccflags

    def get_ldflags(self):
        return self.ldflags

    def get_include(self):
        return self.include

    def get_libs(self):
        return self.libs

    def get_object_files(self):
        return self.object_files

    def get_lib_search_paths(self):
        return self.libsearchpaths

    def get_id(self) -> str:
        if self.dependency_id:
            return self.dependency_id
        project = self.get_project()
        self.dependency_id = project.action("_asdependency_id")
        return self.get_id()

    def __str__(self) -> str:
        return "ResolvedPathDependency({} id={})".format(self.root_path, self.get_id())


class DependenciesResolver(object):
    def __init__(self, desc: CompilingDesc, target: Target = None, dryrun=False):
        self.desc = desc
        self.root_path = desc.root
        self.expand_path = desc.expand_path
        self.dryrun = dryrun
        self.target = target

    def get_resolvers(self) -> List[DependencyResolver]:
        resolvers_accepts_conf: List[str] = self.desc.config["language_c"].get(
            "resolvers", []
        )
        result: List[DependencyResolver] = []
        for resolver_s in resolvers_accepts_conf:
            resolver = GLOBALSTATE.dependency_resolvers.get(resolver_s, None)
            if resolver:
                if resolver.check_avaliablity(self.desc, self.dryrun):
                    result.append(resolver)
                else:
                    print(
                        'langc: WARN dependency resolver "{}" is not avaliable'.format(
                            resolver_s
                        )
                    )
            else:
                panic(
                    'dependency resolver "{}" not found. All resolvers: {}'.format(
                        resolver_s, GLOBALSTATE.dependency_resolvers
                    )
                )
        return result

    def search_dependency_by_resolvers(
        self, depend: Depend, resolvers: List[DependencyResolver], target: Target = None
    ) -> List[ResolvedDependency]:
        multiple_result = []
        for resolver in resolvers:
            dependency = resolver.resolve(
                self.desc, depend, target=target, dryrun=self.dryrun
            )
            if dependency:
                multiple_result.append(dependency)
        return multiple_result

    def resolve(
        self,
        dependency: Depend,
        resolvers: List[DependencyResolver],
        target: Target = None,
    ):
        if dependency.dtype == "direct":
            result = self.search_dependency_by_resolvers(
                dependency, resolvers, target=target
            )
            result = list(filter(lambda x: x != None, result))
            if len(result) > 0:
                picked = result[0]
                if len(result) > 1:
                    print(
                        "langc: WARN that {} match multiple packages, the first one will be picked: {}".format(
                            dependency, picked
                        )
                    )
                return picked
            else:
                return None
        elif dependency.dtype == "path":
            project_path = Path(self.expand_path(dependency.path))
            if project_path.exists():
                d_project: Makru = create_makru("-F{}".format(project_path.absolute()))
                d_project_sandbox = d_project.build_sandbox()
                if d_project_sandbox.is_action_exists("_asdependency_info"):
                    target_project_info: Dict[str, str] = d_project.action(
                        "_asdependency_info"
                    )
                    true_or_panic(
                        target_project_info["type"] == "library",
                        "could not depend on a non-library project: {}".format(
                            dependency
                        ),
                    )
                    return ResolvedPathDependency(
                        str(project_path),
                        dryrun=self.dryrun,
                        vars=self.desc.gvars,
                        depend=dependency,
                    )
                else:
                    panic(
                        'Project "{}" does not support denpendency protocol'.format(
                            project_path
                        )
                    )
            else:
                return None
        else:
            panic("unsupported dependency type: {}".format(dependency))


class CompilingState(object):
    def __init__(
        self,
        desc: CompilingDesc,
        *,
        output_folder_name: str = None,
        dryrun=False,
        target: Target = None
    ):
        if not output_folder_name:
            output_folder_name = "./build"
        self.desc = desc
        self.output_path = desc.expand_path(output_folder_name)
        self.plugin_config: Dict[str, Any] = desc.config["language_c"]
        self.shared_ccflags: List[str] = []
        self.shared_includes: List[str] = []
        self.ldflags: List[str] = []
        self.libs: List[str] = []
        self.extenal_object_files: List[str] = []
        self.lib_search_paths: List[str] = []
        self.dryrun = dryrun
        self.c_standard = self.plugin_config.get("std", None)
        self.target = target
        self._build_store: Store = None
        self.rebuild: bool = desc.gvars.get("rebuild", False)
        self.rebuild_dependencies: bool = desc.gvars.get("rebuild-all", False)
        if self.rebuild_dependencies:
            self.rebuild = True
        self.dependencies_build_message = desc.gvars.get(
            "dependencies-build-message", False
        )
        self.check_defaults()

    def check_defaults(self):
        resolver_names = self.desc.config["language_c"].get("resolvers", None)
        if not resolver_names:
            self.desc.config["language_c"]["resolvers"] = ["pkgconfig", "system"]

    @property
    def build_store(self) -> Store:
        if not self._build_store:
            if not self.dryrun:
                self._build_store = Sandbox.make_store_under(
                    self.output_path, "build"
                )  # This is just a workaround
            else:
                self._build_store = Store(":memory:")
        return self._build_store

    @property
    def info_store(self) -> Store:
        return self.desc.sandbox.info_store

    def get_output_filename(self, name: str):
        return self.output_path + "/" + name

    def get_source_files(self) -> List[str]:
        source_files: List[str] = self.plugin_config["sources"]
        result: List[str] = []
        for p in map(lambda x: self.desc.expand_path(x), source_files):
            path = self.desc.expand_path(p)
            if Path(path).exists():
                result.append(path)
            else:
                result += filter(
                    lambda x: not Path(x).is_dir(), glob(path, recursive=True)
                )
        return result

    def resolve_project_config(self):
        include_path = self.desc.expand_path(
            self.plugin_config.get("include", "./include")
        )
        if Path(include_path).exists():
            self.shared_includes.append(include_path)

    def resolve_dependencies(
        self,
    ):  # TODO： make this method to use resolve_all_dependencies
        dependencies_resolver = DependenciesResolver(self.desc, dryrun=self.dryrun)
        external_resolvers = dependencies_resolver.get_resolvers()
        result = []
        for depend in self.desc.dependencies:
            resolved_dependency = dependencies_resolver.resolve(
                depend, external_resolvers, self.target
            )
            if not resolved_dependency:
                panic(
                    "langc: dependency {} not found, resolvers: {}".format(
                        depend, external_resolvers
                    )
                )
            else:
                result.append(resolved_dependency)
        return result

    def resolve_all_dependencies(self) -> List[Tuple[Depend, ResolvedDependency]]:
        dependencies_resolver = DependenciesResolver(self.desc, dryrun=self.dryrun)
        external_resolvers = dependencies_resolver.get_resolvers()
        result = []
        for depend in self.desc.dependencies:
            resolved_dependency = dependencies_resolver.resolve(
                depend, external_resolvers, self.target
            )
            result.append((depend, resolved_dependency))
        return result

    def build_dependencies(self, resolved_dependencies: Iterable[ResolvedDependency]):
        for resolved_dependency in resolved_dependencies:
            if self.dependencies_build_message:
                print("langc: enter {}".format(resolved_dependency.depend))
            resolved_dependency.do_build(rebuild_all=self.rebuild_dependencies)
            if self.dependencies_build_message:
                print("langc: exit {}".format(resolved_dependency.depend))

    def setup_dependencies_flags(
        self, resolved_dependencies: Iterable[ResolvedDependency]
    ):
        for resolved_dependency in resolved_dependencies:
            self.shared_ccflags += resolved_dependency.get_ccflags()
            self.ldflags += resolved_dependency.get_ldflags()
            include = resolved_dependency.get_include()
            if include:
                self.shared_includes.append(include)
            self.libs += resolved_dependency.get_libs()
            self.lib_search_paths += resolved_dependency.get_lib_search_paths()
            self.extenal_object_files += resolved_dependency.get_object_files()

    def get_output_path(self, name=".", file=True) -> Path:
        target: Path = Path(self.output_path) / name
        if self.dryrun:
            return target
        elif file and (not target.parent.exists()):
            target.parent.mkdir(parents=True, exist_ok=True)
        elif (not file) and (not target.exists()):
            target.mkdir(parents=True, exist_ok=True)
        return target

    def get_final_library_name(self) -> str:
        return self.desc.name + ".so"

    def get_final_static_library_name(self) -> str:
        return self.desc.name + ".a"

    def get_final_executable_name(self) -> str:
        name = self.plugin_config.get("executable_name", None)
        if name:
            return name
        else:
            return self.desc.name

    def select_compiler(
        self,
        perfer: str = None,
        perfer_is_required: bool = False,
        panic_if_not_found: bool = True,
        **kwargs
    ) -> Optional[Compiler]:
        from .compilers import guess_compiler_from_version_info

        COMPILERS = GLOBALSTATE.compilers
        user_perfer = self.desc.gvars.get("compiler", None)
        if user_perfer:
            if user_perfer in COMPILERS and COMPILERS[user_perfer].is_avaliable(
                target=self.target
            ):
                return COMPILERS[user_perfer](**kwargs)
        if perfer and COMPILERS[perfer].is_avaliable(target=self.target):
            return COMPILERS[perfer](**kwargs)
        elif perfer_is_required:
            panic("requested must-have compiler {} is unavaliable".format(perfer))
        path = check_binary("cc")  # Genernally "cc" is the "default compiler" alias
        if path:
            default_compiler = guess_compiler_from_version_info(path)
            if default_compiler and default_compiler.is_avaliable(target=self.target):
                return default_compiler(**kwargs)
        for k in COMPILERS.keys():
            compiler = COMPILERS[k]
            if compiler.is_avaliable(target=self.target):
                return compiler(**kwargs)
        if panic_if_not_found:
            panic(
                "no any supported compilers is avaliable: {}".format(
                    ", ".join(COMPILERS.keys())
                )
            )
        return None

    def select_linker(
        self,
        perfer: str = None,
        perfer_is_required: bool = False,
        panic_if_not_found: bool = True,
        **kwargs
    ) -> Optional[Linker]:
        LINKERS = GLOBALSTATE.linkers
        if perfer and LINKERS[perfer].is_avaliable(target=self.target):
            return LINKERS[perfer](**kwargs)
        elif perfer_is_required:
            panic("requested must-have linker {} is unavaliable".format(perfer))
        for k in LINKERS.keys():
            linker = LINKERS[k]
            if linker.is_avaliable(target=self.target):
                return linker(**kwargs)
        if panic_if_not_found:
            panic(
                "no any supported linkers is avaliable: {}".format(
                    ", ".join(LINKERS.keys())
                )
            )
        return None

    def select_archiver(
        self,
        perfer: str = None,
        perfer_is_required: bool = False,
        panic_if_not_found: bool = True,
        **kwargs
    ) -> Optional[Linker]:
        ARCHIVERS = GLOBALSTATE.archivers
        if perfer and ARCHIVERS[perfer].is_avaliable(target=self.target):
            return ARCHIVERS[perfer](**kwargs)
        elif perfer_is_required:
            panic("requested must-have archiver {} is unavaliable".format(perfer))
        for k in ARCHIVERS.keys():
            ar = ARCHIVERS[k]
            if ar.is_avaliable(target=self.target):
                return ar(**kwargs)
        if panic_if_not_found:
            panic(
                "no any supported linkers is avaliable: {}".format(
                    ", ".join(ARCHIVERS.keys())
                )
            )
        return None

    def is_as_static_library(self) -> bool:
        return (self.desc.type == "library") and self.desc.gvars.get(
            "static-library", False
        )

    def get_workflow(
        self,
    ) -> Workflow:  # FIXME(rubicon): maybe it's a mypy's fault # rubicon note: I don't know why mypy report it as "missing return statement"
        t = self.desc.type
        if t in GLOBALSTATE.workflows:
            return GLOBALSTATE.workflows[t]
        else:
            panic("could not found workflow for type '{}'".format(t))

    @cached_property
    def table_dependency_ids(self):
        return self.build_store.get_table("dependency_ids")

    def declare_dependency_change(self, depend_str: str, dependency_id: str):
        self.table_dependency_ids.set(depend_str, dependency_id)

    def check_if_dependency_changed(self, depend_str: str, dependency_id: str):
        if not dependency_id:
            return False
        saved_id = self.table_dependency_ids.get(depend_str)
        if not saved_id:
            return False
        return saved_id != dependency_id

    def filter_changed_dependencies(
        self,
        dependencies: Iterable[ResolvedDependency],
        panic_if_non_resolved: bool = False,
    ) -> Iterator[ResolvedDependency]:
        for rdepend in dependencies:
            depend = rdepend.depend
            if panic_if_non_resolved and (not rdepend):
                panic("dependency '{}' is not resolved".format(depend))
            elif rdepend and self.check_if_dependency_changed(
                str(depend), rdepend.get_id()
            ):
                yield rdepend

    def list_changed_dependencies(
        self,
        dependencies: Iterable[ResolvedDependency],
        panic_if_non_resolved: bool = False,
    ):
        return list(
            self.filter_changed_dependencies(
                dependencies, panic_if_non_resolved=panic_if_non_resolved
            )
        )

    def declare_dependency_changes(self, dependencies: List[Tuple[str, str]]):
        for depend_s, d_id in dependencies:
            self.declare_dependency_change(depend_s, d_id)

    def get_self_include_path(self):
        return self.desc.expand_path(self.plugin_config.get("include", "./include"))

    @classmethod
    def build(cls, desc: CompilingDesc):
        target_option = desc.gvars.get("target", None)
        target = None
        if target_option:
            target = Target(Target.parse_triplet(target_option))
        return cls(
            desc,
            output_folder_name=desc.gvars.get("build-path", None),
            dryrun=desc.gvars.get("dryrun", False),
            target=target,
        )


def compile(desc: CompilingDesc):
    state = CompilingState.build(desc)
    if state.dryrun:
        print("!! no actual changes will be made in dry-run mode")
    plugin_config: Dict[str, Any] = desc.config["language_c"]
    if desc.type in GLOBALSTATE.workflows:
        workflow = GLOBALSTATE.workflows[desc.type]
        result = workflow.enter(desc.type, state)
        if result != 0:
            panic("langc: got non-zero return code {}, exit".format(result))
    else:
        panic(
            "unsupported project type: {}, current workflows: {}".format(
                desc.type, ",".join(GLOBALSTATE.workflows)
            )
        )


def resolve_dependencies(desc: CompilingDesc):
    state = CompilingState.build(desc)
    deps = state.resolve_all_dependencies()
    print("Dependencies:")
    for d, rd in deps:
        print("  {}  {}".format(d, rd if rd else "NOT FOUND"))


def run(desc: CompilingDesc):
    compile(desc)
    state = CompilingState.build(desc)
    state.get_workflow().run(desc.type, state)


def envinfo(desc: CompilingDesc):
    from .compilers import guess_compiler_from_version_info

    state = CompilingState.build(desc)
    print("Environment:")
    print("\tTarget: {}".format(state.target.triplet if state.target else None))
    print(
        "\tCompiler (default selected): {}".format(
            state.select_compiler(panic_if_not_found=False)
        )
    )
    guess_compiler_path = check_binary("cc")
    if guess_compiler_path:
        guess_compiler = guess_compiler_from_version_info(guess_compiler_path)
    print(
        "\tCompiler (guess from 'cc'): {}".format(
            str(guess_compiler(path=guess_compiler_path) if guess_compiler else None)
        )
    )


def on_sandbox_load(sandbox: Sandbox):
    sandbox.plugbox.runhook("langc_startup", GLOBALSTATE)

    @sandbox.add_private_action("_asdependency_include")
    def _asdependency_include(desc: CompilingDesc):
        plugin_config: Dict[str, Any] = desc.config["language_c"]
        return desc.expand_path(plugin_config.get("include", "./include"))

    @sandbox.add_private_action("_asdependency_info")
    def _asdependency_info(desc: CompilingDesc):
        return {
            "type": desc.type,
            "version": desc.sandbox.config.get("version", None),
        }

    @sandbox.add_private_action("_asdependency_ccflags")
    def _asdependency_ccflags(desc: CompilingDesc):
        return []

    @sandbox.add_private_action("_asdependency_ldflags")
    def _asdependency_ldflags(desc: CompilingDesc):
        return []

    @sandbox.add_private_action("_asdependency_object_files")
    def _asdependency_object_files(desc: CompilingDesc):
        is_static_lib = desc.gvars.get("static-library", False)
        if is_static_lib:
            return [
                str(
                    Path(desc.gvars["build-path"])
                    / CompilingState(desc).get_final_static_library_name()
                )
            ]
        else:
            return []

    @sandbox.add_private_action("_asdependency_libsearchpaths")
    def _asdependency_libsearchpaths(desc: CompilingDesc):
        is_static_lib = desc.gvars.get("static-library", False)
        if is_static_lib:
            return []
        else:
            return [str(Path(desc.gvars["build-path"]))]

    @sandbox.add_private_action("_asdependency_libs")
    def _asdependency_libs(desc: CompilingDesc):
        is_static_lib = desc.gvars.get("static-library", False)
        if is_static_lib:
            return []
        else:
            return [desc.name]

    @sandbox.add_private_action("_asdependency_id")
    def _asdependency_id(desc: CompilingDesc):
        state = CompilingState.build(desc)
        return state.get_workflow().get_id(state.desc.type, state)


def after_configuration_load(makru: Makru):
    makru.define_option(
        "use-system-package",
        OptionDescription(
            plugin_name="makru_langc",
            description="allow 'system' resolver respond to specific packages which is defined with version",
            multiple=True,
        ),
    )
    makru.define_option(
        "SYSPKG",
        OptionDescription(plugin_name="makru_langc", mix_into="use-system-package"),
    )
    makru.define_option(
        "rebuild",
        OptionDescription(
            plugin_name="makru_langc",
            description="force to rebuild this project",
            bool_value=True,
        ),
    )
    makru.define_option(
        "rebuild-all",
        OptionDescription(
            plugin_name="makru_langc",
            description="force to rebuild this project and dependencies, this option will override the value of 'rebuild'",
            bool_value=True,
        ),
    )
    makru.define_option(
        "dryrun",
        OptionDescription(
            plugin_name="makru_langc",
            description="mark this run is dry run, which should not make any actual effect",
            bool_value=True,
        ),
    )
    makru.define_option(
        "static-library",
        OptionDescription(
            plugin_name="makru_langc",
            description="treat the library project as static library",
            bool_value=True,
        ),
    )
    makru.define_option(
        "build-path",
        OptionDescription(
            plugin_name="makru_langc", description="set build output directory"
        ),
    )
    makru.define_option(
        "dependencies-build-messgae",
        OptionDescription(
            plugin_name="makru_langc",
            description="control if makru_langc should print the messages about starting or exiting a dependency building, true means to print these messages (default: false)",
            bool_value=True,
        ),
    )
    makru.define_option(
        "compiler",
        OptionDescription(
            plugin_name="makru_langc",
            description="define the compiler name in makru's supported list you perfer, by default it will use the default path of the implementation, currently supported names: {}".format(
                ", ".join(["cc", "clang"])
            ),
        ),
    )


def langc_startup(pstate: PluginGlobalState):
    from . import pkgconf, compilers, linkers, workflows, syspkg

    pkgconf.langc_startup(pstate)
    compilers.langc_startup(pstate)
    linkers.langc_startup(pstate)
    workflows.langc_startup(pstate)
    syspkg.langc_startup(pstate)
