from . import DependencyResolver, ResolvedDependency, PluginGlobalState, Target
from makru import CompilingDesc, Depend
from makru.helpers import exprog, check_binary, match_version, is_semver
from typing import List, Optional
import re
from hashlib import sha256


class PkgconfResolvedDependency(ResolvedDependency):
    def __init__(self, ccflags, *, depend: Depend, modversion: str) -> None:
        self.ccflags: List[str] = ccflags
        self.dep_id = sha256(
            bytes((" ".join(ccflags)) + modversion, encoding="utf8")
        ).hexdigest()
        super().__init__(depend)

    def get_ccflags(self):
        return self.ccflags

    def get_id(self):
        return self.dep_id

    def __str__(self) -> str:
        return "PkgconfResolvedDependency(ccflags={}, id={})".format(
            " ".join(self.ccflags), self.dep_id
        )


def is_module_exists(name: str):
    popen = exprog(["pkgconf", name, "--exists"])
    if popen.wait() == 0:
        return True
    else:
        return False


def get_module_version(name: str):
    popen = exprog(["pkgconf", name, "--modversion"])
    if popen.wait() == 0:
        return popen.stdout.read().splitlines()[0]
    else:
        return None


def get_module_ccflags(name: str):
    popen = exprog(["pkgconf", name, "--cflags", "--libs"])
    if popen.wait() == 0:
        return popen.stdout.read().splitlines()[0].strip().split(" ")
        # why here's a .strip(): sometimes the result will have trailing spaces, which make some empty str in the end of returned list.
        # ... GCC will treat the empty string as a file path, and will report "no such file or directory".
        # ... CLANG doesn't.
    else:
        return None


def get_package_dependency(
    name: str, depend: Depend, modversion: str
) -> PkgconfResolvedDependency:
    return PkgconfResolvedDependency(
        ccflags=get_module_ccflags(name), depend=depend, modversion=modversion
    )


COMPARE_OPS = [">", "==", "<", ">=", "<="]


def is_compare_op(s: str):
    for op in COMPARE_OPS:
        if s.startswith(op):
            return True
    return False


class PkgconfResolver(DependencyResolver):
    def resolve(
        self, desc: CompilingDesc, depend: Depend, target: Target = None, dryrun=False
    ) -> Optional["ResolvedDependency"]:
        if target:
            return None
        if is_module_exists(depend.name):
            version = get_module_version(depend.name)
            if depend.version:
                if is_compare_op(depend.version):
                    conds = depend.version.split(",")
                    for cond in conds:
                        if not match_version(version, cond):
                            return None
                else:
                    m = re.match(depend.version, version, re.IGNORECASE)
                    if not m:
                        print(
                            "langc.pkgconf: {} was found, but the version {} could not match requested {}".format(
                                depend.name, version, depend.version
                            )
                        )
                        return None
            return get_package_dependency(depend.name, depend, version)
        return None

    def check_avaliablity(self, desc: CompilingDesc, dryrun=False) -> bool:
        return check_binary("pkgconf") != None


def langc_startup(pstate: PluginGlobalState):
    pstate.register_dependency_resolver("pkgconfig", PkgconfResolver())
