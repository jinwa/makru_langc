from io import StringIO
from typing import Optional, Type
from makru import panic
from makru.helpers import check_binary, shell
from . import CompilingFlags, PluginGlobalState, Target, generate_shell_show, Compiler


def guess_compiler_from_version_info(binary_path: str) -> Optional[Type[Compiler]]:
    stdout = StringIO()
    shell([binary_path, "--version"], printf=stdout.write)
    output = stdout.getvalue()
    lines = output.splitlines()
    fstline = lines[
        1
    ]  # the original first line is the commandline, not the output from executable
    if fstline.startswith("clang"):
        return ClangCompiler
    elif "GCC" in fstline:
        return CommonCompiler
    else:
        return None


class CommonCompiler(Compiler):
    def __init__(self, path=None) -> None:
        self.path = path if path else check_binary("cc")

    def compile(
        self, source_file: str, output_name: str, flags: CompilingFlags, dryrun=False
    ):
        if flags.target:
            panic("CommonCompiler does not support cross-compiling")
        autogen_ccflags = []
        for v in flags.includes:
            autogen_ccflags.append("-I{}".format(v))
        for v in flags.libs:
            autogen_ccflags.append("-l{}".format(v))
        for v in flags.libsearchs:
            autogen_ccflags.append("-L{}".format(v))
        if flags.PIC:
            autogen_ccflags.append("-fPIC")
        if flags.c_std:
            autogen_ccflags.append("-std={}".format(flags.c_std))
        command = [
            self.path,
            "-c",
            source_file,
            "-o",
            output_name,
            *autogen_ccflags,
            *flags.ccflags,
        ]
        if dryrun:
            print(generate_shell_show(command, cwd=flags.cwd, env=flags.environment))
            return 0
        else:
            return shell(command, cwd=flags.cwd, env=flags.environment)

    @classmethod
    def is_avaliable(cls, *, target: Optional[Target]):
        if target:
            return False
        return check_binary("cc") != None

    def __str__(self) -> str:
        return 'CommonCompiler("gcc-like", path={})'.format(self.path)


class ClangCompiler(Compiler):
    def __init__(self, path=None) -> None:
        self.path = path if path else check_binary("clang")

    @classmethod
    def is_avaliable(cls, *, target: Optional["Target"]):
        return check_binary("clang") != None

    def compile(
        self, source_file: str, output_name: str, flags: CompilingFlags, dryrun=False
    ):
        autogen_ccflags = []
        for v in flags.includes:
            autogen_ccflags.append("-I{}".format(v))
        for v in flags.libs:
            autogen_ccflags.append("-l{}".format(v))
        for v in flags.libsearchs:
            autogen_ccflags.append("-L{}".format(v))
        if flags.PIC:
            autogen_ccflags.append("-fPIC")
        if flags.c_std:
            autogen_ccflags.append("-std={}".format(flags.c_std))
        if flags.target:
            autogen_ccflags.append(
                "--target={}".format(Target.triplet2str(flags.target.triplet))
            )
        command = [
            self.path,
            "-c",
            source_file,
            "-o",
            output_name,
            *autogen_ccflags,
            *flags.ccflags,
        ]
        if dryrun:
            print(generate_shell_show(command, cwd=flags.cwd, env=flags.environment))
            return 0
        else:
            return shell(command, cwd=flags.cwd, env=flags.environment)

    def __str__(self) -> str:
        return "ClangCompiler(path={})".format(self.path)


def langc_startup(pstate: PluginGlobalState):
    pstate.register_compiler("clang", ClangCompiler)
    pstate.register_compiler("cc", CommonCompiler)
