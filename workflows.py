import os
from pathlib import Path
from typing import Generator, Iterable, List

from makru.makru import panic
from makru import true_or_panic
from makru.helpers import get_file_hash_sha224
from . import (
    CompilingState,
    PluginGlobalState,
    ResolvedDependency,
    Workflow,
    LinkerFlags,
    CompilingFlags,
)
from functools import cache

FILEHASH_TABLE_NAME = "filehashes"


class CommonWorkflow(Workflow):
    def __init__(self):
        pass

    def enter(self, type: str, state: CompilingState):
        if type == "library":
            if state.desc.gvars.get("static-library", False):
                return self.do_static_library(state)
            else:
                return self.do_library(state)
        elif type == "executable":
            return self.do_executable(state)

    def get_id(self, type: str, state: CompilingState):
        path = None
        if type == "library":
            if state.is_as_static_library():
                path = Path(
                    state.get_output_path(state.get_final_static_library_name())
                )
            else:
                path = Path(state.get_output_path(state.get_final_library_name()))
        else:
            path = Path(state.get_output_path(state.get_final_executable_name()))
        if path.exists():
            return self.get_filehash(str(path))
        else:
            return None

    @staticmethod
    def filehashes_table(store):
        return store.get_table(FILEHASH_TABLE_NAME)

    @staticmethod
    @cache
    def get_filehash(fpath):
        return get_file_hash_sha224(fpath)

    def check_if_new_file(self, table, filepath):
        tvalue = table.get(filepath)
        if not tvalue:
            return True
        hash = self.get_filehash(filepath)
        return tvalue != hash

    def declare_file(self, table, fpath):
        table.set(fpath, self.get_filehash(fpath))

    @staticmethod
    def get_filenames_under(path):
        if Path(path).exists():
            for p in Path(path).glob("**"):
                if p.is_file():
                    yield p

    @classmethod
    def list_filenames_under(cls, path):
        return list(cls.get_filenames_under(path))

    def filter_changed_file_by_names(
        self, store, names: Iterable[str]
    ) -> Generator[str, None, None]:
        table = self.filehashes_table(store)
        for name in names:
            if self.get_filehash(name) != table.get(name):
                yield name

    def list_changed_file_by_names(self, store, names: Iterable[str]) -> List[str]:
        return list(self.filter_changed_file_by_names(store, names))

    def declare_changed_files(self, store, names: Iterable[str]):
        table = self.filehashes_table(store)
        for name in names:
            table.set(name, self.get_filehash(name))

    def do_library(self, state: CompilingState):
        compiler = state.select_compiler()
        linker = state.select_linker()
        bstore = state.build_store
        filehashes_t = self.filehashes_table(bstore)
        state.resolve_project_config()
        dependencies = state.resolve_dependencies()
        state.build_dependencies(
            dependencies
        )  # We still need to call build for the whole dependencies tree, let resolver manage if a real rebuild needed
        changed_dependencies = state.list_changed_dependencies(
            dependencies, panic_if_non_resolved=True
        )
        have_changed_dependencies = len(changed_dependencies) > 0
        state.setup_dependencies_flags(dependencies)
        changed_include_names = self.list_changed_file_by_names(
            bstore, self.get_filenames_under(state.get_self_include_path())
        )
        have_changed_include = len(changed_include_names) > 0
        output_objects_paths = []
        # Compiling
        source_files = state.get_source_files()
        true_or_panic(len(source_files) > 0, "there is no source files matched")
        ccflags = CompilingFlags(
            PIC=True,
            includes=state.shared_includes,
            cwd=state.desc.root,
            ccflags=state.shared_ccflags,
            c_std=state.c_standard,
            target=state.target,
        )
        have_any_changes = False
        if state.rebuild:
            have_any_changes = True
            have_changed_include = True
            have_changed_dependencies = True
        for filepath in source_files:
            path = Path(filepath)
            output_name = str(
                state.get_output_path(
                    path.relative_to(state.desc.root).with_suffix(".o")
                )
            )
            if (
                have_changed_dependencies
                or have_changed_include
                or (not Path(output_name).exists())
                or self.check_if_new_file(filehashes_t, filepath)
            ):
                result = compiler.compile(
                    filepath, output_name, ccflags, dryrun=state.dryrun
                )
                if result != 0:
                    return result
                else:
                    self.declare_file(filehashes_t, filepath)
                    have_any_changes = True
            output_objects_paths.append(output_name)
        if have_changed_include:
            self.declare_changed_files(bstore, changed_include_names)
        # Linking
        result = 0
        ld_output_filename = state.get_output_filename(state.get_final_library_name())
        if have_any_changes or (not Path(ld_output_filename).exists()):
            ldflags = LinkerFlags(
                shared=True,
                ldflags=[
                    *map(lambda x: "-l{}".format(x), state.libs),
                    *map(lambda x: "-L{}".format(x), state.lib_search_paths),
                    *state.ldflags,
                ],
                target=state.target,
            )
            result = linker.link(
                [*output_objects_paths, *state.extenal_object_files],
                ld_output_filename,
                ldflags,
                dryrun=state.dryrun,
            )
        if have_changed_dependencies:
            # Declare dependency changes (or we still need to compile the whole source tree)
            state.declare_dependency_changes(
                map(lambda d: (str(d.depend), d.get_id()), changed_dependencies)
            )
        return result

    def do_static_library(self, state: CompilingState):
        compiler = state.select_compiler()
        linker = state.select_archiver()
        bstore = state.build_store
        filehashes_t = bstore.get_table(FILEHASH_TABLE_NAME)
        state.resolve_project_config()
        dependencies = state.resolve_dependencies()
        state.build_dependencies(dependencies)
        changed_dependencies = state.list_changed_dependencies(
            dependencies, panic_if_non_resolved=True
        )
        have_dependency_changes = len(changed_dependencies) > 0
        state.setup_dependencies_flags(dependencies)
        changed_include_names = self.list_changed_file_by_names(
            bstore, self.get_filenames_under(state.get_self_include_path())
        )
        have_changed_include = len(changed_include_names) > 0
        output_objects_paths = []
        # Compiling
        source_files = state.get_source_files()
        true_or_panic(len(source_files) > 0, "there is no source files matched")
        ccflags = CompilingFlags(
            PIC=True,
            includes=state.shared_includes,
            libs=state.libs,
            libsearchs=state.lib_search_paths,
            cwd=state.desc.root,
            ccflags=state.shared_ccflags,
            c_std=state.c_standard,
            target=state.target,
        )
        have_any_change = False
        if state.rebuild:
            have_any_change = True
            have_changed_include = True
            have_dependency_changes = True
        for filepath in source_files:
            path = Path(filepath)
            output_name = str(
                state.get_output_path(
                    path.relative_to(state.desc.root).with_suffix(".o")
                )
            )
            if (
                have_dependency_changes
                or have_changed_include
                or (not Path(output_name).exists())
                or self.check_if_new_file(filehashes_t, filepath)
            ):
                result = compiler.compile(
                    filepath, output_name, ccflags, dryrun=state.dryrun
                )
                if result != 0:
                    return result
                else:
                    self.declare_file(filehashes_t, filepath)
                    have_any_change = True
            output_objects_paths.append(output_name)
        if have_changed_include:
            self.declare_changed_files(bstore, changed_include_names)
        # Linking (archive object files)
        result = 0
        ld_output_name = state.get_output_filename(
            state.get_final_static_library_name()
        )
        if have_any_change or (not Path(ld_output_name).exists()):
            ldflags = LinkerFlags(ldflags=[], target=state.target)
            result = linker.link(
                [*output_objects_paths, *state.extenal_object_files],
                ld_output_name,
                ldflags,
                dryrun=state.dryrun,
            )  # ignore all ldflags
        if have_dependency_changes:
            state.declare_dependency_changes(
                map(lambda d: (str(d.depend), d.get_id()), changed_dependencies)
            )
        return result

    def do_executable(self, state: CompilingState):
        compiler = state.select_compiler()
        linker = state.select_linker()
        bstore = state.build_store
        filehashes_t = bstore.get_table(FILEHASH_TABLE_NAME)
        state.resolve_project_config()
        dependencies = state.resolve_dependencies()
        state.build_dependencies(dependencies)
        changed_dependencies = state.list_changed_dependencies(
            dependencies, panic_if_non_resolved=True
        )
        have_dependency_changes = len(changed_dependencies) > 0
        state.setup_dependencies_flags(dependencies)
        changed_include_names = self.list_changed_file_by_names(
            bstore, self.get_filenames_under(state.get_self_include_path())
        )
        have_changed_include = len(changed_include_names) > 0
        output_objects_paths = []
        # Compiling
        source_files = state.get_source_files()
        true_or_panic(len(source_files) > 0, "there is no source files matched")
        ccflags = CompilingFlags(
            includes=state.shared_includes,
            cwd=state.desc.root,
            ccflags=state.shared_ccflags,
            c_std=state.c_standard,
            target=state.target,
        )
        have_any_change = False
        if state.rebuild:
            have_any_change = True
            have_changed_include = True
            have_dependency_changes = True
        for filepath in source_files:
            path = Path(filepath)
            output_name = str(
                state.get_output_path(
                    path.relative_to(state.desc.root).with_suffix(".o")
                )
            )
            if (
                have_dependency_changes
                or have_changed_include
                or (not Path(output_name).exists())
                or self.check_if_new_file(filehashes_t, filepath)
            ):
                result = compiler.compile(
                    filepath, output_name, ccflags, dryrun=state.dryrun
                )
                if result != 0:
                    return result
                else:
                    self.declare_file(filehashes_t, filepath)
                    have_any_change = True
            output_objects_paths.append(output_name)
        if have_changed_include:
            self.declare_changed_files(bstore, changed_include_names)
        # Linking
        result = 0
        ld_output_name = state.get_output_filename(state.get_final_executable_name())
        if have_any_change or (not Path(ld_output_name).exists()):
            ldflags = LinkerFlags(
                ldflags=[
                    *map(lambda x: "-l{}".format(x), state.libs),
                    *map(lambda x: "-L{}".format(x), state.lib_search_paths),
                    *state.ldflags,
                ],
            )
            result = linker.link(
                [*output_objects_paths, *state.extenal_object_files],
                ld_output_name,
                ldflags,
                dryrun=state.dryrun,
            )
        if have_dependency_changes:
            state.declare_dependency_changes(
                map(lambda d: (str(d.depend), d.get_id()), changed_dependencies)
            )
        return result

    def run(self, project_type: str, state: CompilingState):
        if project_type != "executable":
            panic("common_workflow: only 'executable' type support action 'run'")
        exe_path = state.get_output_path(state.get_final_executable_name())
        if (
            exe_path.exists()
        ):  # In fact it might be there if the method called by makru, but we need still to check it because makru_langc allow pass option --dryrun with :run action.
            os.execl(exe_path, exe_path)
        else:
            panic("could not found {}".format(exe_path))


def langc_startup(pstate: PluginGlobalState):
    pstate.register_workflow("library", CommonWorkflow())
    pstate.register_workflow("executable", CommonWorkflow())
