# Custom Hooks
## `langc_startup(pstate: PluginGlobalState)`
Calling while makru_langc is starting up.
`PluginGlobalState` consist of compilers, linkers, archivers, workflows and dependency resolvers.
