# Bulit-in Dependency Resolvers

To use dependency resolvers (for both built-in and third-party), just add them to field `language_c.resolvers` in your configuration.

````yaml
language_c:
  resolvers:
    - pkgconfig
    - system
````

The dependency you requested will walkthough the list from-top-to-bottom and the first result will be picked. For example, depends on the config above, if both `pkgconfig` and `system` have the result for `glib`, the `pkgconfig`'s will be picked. You can use action `resolve_dependencies` to find out which package is picked.

## `pkgconfig`
Find package though [pkgconfig](https://www.freedesktop.org/wiki/Software/pkg-config/) CLI. The plugin sets the pkgconfig binary called "pkgconf".

````yaml
dependencies:
  - glib@>=1.0.0,<2.0.0
language_c:
  reoslvers:
    - pkgconfig
````

### Version matching
This resolver supports version matching in two ways. When you are using matching expressions, which start with any matching operator (>, <, =, >=, <=), it will evaluate the comparing: ">=1.0.0,<2.0.0" will match any version which is not less than 1.0.0 and less than 2.0.0. Matching expressions could be separated by `,` without any space between characters, that means all the expressions should be truth together.
If you are not using any operator in the start, this resolver will treat it as a regular expression with Python standard and ignored lettercase.


## `system`
Most *unix put the includes and libraries in defined directory. This resolver can find the header files from the includes directory and the shared libraries from libraries directory. This resolver does not respond to the requests which contains version (for example: `glib@>=1.0.0,<2.0.0`), you must only write the name if you want to the resolver takes effects:

````yaml
dependencies:
  - glib
language_c:
  reoslvers:
    - system
````

This resolver does not have default paths on the platform rather linux. On linux, the default include directory is `/usr/include` and `/usr/local/include`, the default libraries directory is `/usr/lib` and `/usr/local/lib`, plus `/usr/lib64` and `/usr/local/lib64` if the python executable, which runs the resolver, is "64 bits" (the max number of int is greater than 2^32, this dectection is recommended by python document).
