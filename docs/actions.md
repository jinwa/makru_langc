# Actions

These actions are public actions, you must define the linking in configuration before using:
````yaml
actions:
  run: makru_langc:run
  resolve_dependencies: makru_langc:resolve_dependencies
````

## `makru_langc:resolve_dependencies`
Resolve (direct) dependencies and print infomation.

Sample:
````
(venv) $ makru :resolve_dependencies
Dependencies:
  pth None  NOT FOUND
  pthread None  ResolvedSystemPackage(include=/usr/include, libs=pthread, lib_searchs=/usr/lib64)
  glib-2.0 >2.66.0,<2.67.0  PkgconfResolvedDependency(-I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -lglib-2.0)
  liburing None  PkgconfResolvedDependency(-luring)
````

## `makru_langc:run`
Compile the project and execute it without arguments. The detailed behaviour depends on workflow. The bulit-in "common workflow" will run the executable if the project type is "executable".

````
(venv) $ makru :run
$ /usr/bin/clang -c /path/to/makru-langc-sample/library/library.c -o /path/to/makru-langc-sample/library/build/library.o -I/path/to/makru-langc-sample/library/include -fPIC -std=c11
$ /usr/bin/ar -rv /path/to/makru-langc-sample/library/build/ExampleLibrary.a /path/to/makru-langc-sample/library/build/library.o
a - /path/to/makru-langc-sample/library/build/library.o
$ /usr/bin/clang -c /path/to/makru-langc-sample/program.c -o /path/to/makru-langc-sample/build/program.o -I/path/to/makru-langc-sample/library/include -std=c11 -I/usr/include/glib-1.2 -I/usr/lib64/glib/include -lglib
$ /usr/bin/clang /path/to/makru-langc-sample/build/program.o /path/to/makru-langc-sample/library/build/ExampleLibrary.a -o /path/to/makru-langc-sample/build/example
Hello, world
Hello, Jack Cooper
````

## `makru_langc:envinfo`
Output infomation about environment.
