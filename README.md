# makru_langc
It's a [makru](https://gitlab.com/jinwa/makru) plugin for C toolchain support. It's tent to be a alternative to currently had make-like compiling tool (make, cmake, etc.).

With makru_langc, it's just lines YAML to start your C project and you are not to be lost in the sea of configuration. Makru + this plugin make modern taste for C programming experience.

It's still in early stage.

## Documents

- [Demo](https://gitlab.com/jinwa/makru-langc-sample)
- [Bulit-in Actions](docs/actions.md)
- Quick Start
- [Bulit-in Dependency Resolvers](docs/builtin_dep_reslr.md)
- How It Works
- Write a Dependency Resolver for makru_langc
- Contribution Guide

## License
The MIT License. See `LICENSE`.
